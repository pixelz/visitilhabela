<?php
if(isset($data->order_id) && $data->order_id ) {
	
	$order = wc_get_order( $data->order_id );
	$payment_url = $order->get_checkout_payment_url();
}

//echo '<div class="notification closeable success">' . $data->message . '</div>';
if(isset($data->error) && $data->error == true){  ?>
	<div class="booking-confirmation-page booking-confrimation-error">
	<i class="fa fa-exclamation-circle"></i>
	<h2 class="margin-top-30"><?php esc_html_e('Ocorreu algum problema.','listeo_core'); ?></h2>
	<p><?php echo  $data->message  ?></p>
</div>

<?php } else { ?>
<div class="booking-confirmation-page">
	<img src="../images/loading-pagamento.gif">
	<h2 class="margin-top-30"><?php esc_html_e('Obrigado!','listeo_core'); ?></h2>
	<p><?php echo  $data->message  ?></p>

	<?php 
	if(isset($payment_url)) { 
		if(!get_option('listeo_disable_payments')){?>
	<script>
		setTimeout(function() {
			window.location.href = "<?php echo $payment_url; ?>";
		}, 4000);
		</script>
		<a href="<?php echo esc_url($payment_url); ?>" class="button color"><?php esc_html_e('Pay now','listeo_core'); ?></a>
	<?php } 
	}?>

	<?php $user_bookings_page = get_option('listeo_user_bookings_page');  
	if( $user_bookings_page ) : ?>
	<a style="display: none" href="<?php echo esc_url(get_permalink($user_bookings_page)); ?>" class="button"><?php esc_html_e('Minhas experiências','listeo_core'); ?></a>
	<?php endif; ?>
</div>
<?php } ?>

